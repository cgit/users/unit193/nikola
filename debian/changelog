nikola (8.3.0-0vanir1) unstable; urgency=medium

  * New upstream version 8.3.0.
  * d/control: Drop python3-yapsy from Build-Depends, add python3-feedparser.
  * d/rules: Disable failing tests.
  * Update Standards-Version to 4.7.0.

 -- Unit 193 <unit193@unit193.net>  Tue, 23 Apr 2024 00:40:47 -0400

nikola (8.2.3-0vanir1) unstable; urgency=medium

  * New upstream version 8.2.3.
  * d/control:
    - Drop python3-pygal Build-Depends.
    - Update my email address.
  * Update Standards-Version to 4.6.2.

 -- Unit 193 <unit193@unit193.net>  Fri, 05 Aug 2022 01:03:30 -0400

nikola (8.2.0-0vanir1) unstable; urgency=medium

  * New upstream version 8.2.0.
  * d/control: Bump DH compat to 13.
  * d/nikola.manpages: Drop, let upstream's buildsystem handle this.
  * Update Standards-Version to 4.6.0.

 -- Unit 193 <unit193@debian.org>  Wed, 20 Apr 2022 00:14:48 -0400

nikola (8.1.3-0vanir1) unstable; urgency=medium

  * d/watch: Update to find new GitHub releases.
  * New upstream version 8.1.3.
    - Drop upstream patch.

 -- Unit 193 <unit193@debian.org>  Thu, 22 Apr 2021 20:35:02 -0400

nikola (8.1.2-0vanir1) unstable; urgency=medium

  [ Ulises Vitulli ]
  * New Upstream release (Closes: #801796).
  * Refreshed local patches.
  * Prune also Nikola.egg-info
  * Updated debian/copyright on new files
  * Upgrade pkg policy

  [ Unit 193 ]
  * New upstream version 8.1.2.
    - Drop patches, no longer needed.
  * d/compat, d/control:
    - Drop d/compat in favor of debhelper-compat, bump to 12.
  * d/control:
    - Rely on dh-python for Python depends.
    - Update (Build-)Depends for Python 3 version, adding nocheck where useful.
    - Drop X-Python-Version.
    - Set myself as sole maintainer.
    - Update Vcs-* fields for new location.
    - Use 'https' in 'homepage' field.
    - R³: no.
  * d/copyright: Update format spec and attribution.
  * d/nikola.links:
    - Update links for new version and bootstrap, leaving popper.js unlinked.
  * d/rules:
    - Update for Python 3.
    - Set LC_ALL=C.UTF-8 and re-enable tests.
    - Convert new documentation files.
    - Remove extra license files.
    - Drop deprecated get-orig-source target and dpkg-parsechangelog calls.
  * d/watch: Update version to 4.
  * d/p/fix_traceback_on_deploy.patch:
    - Grab an upstream commit to fix traceback on deploy.
  * d/missing-sources, d/NEWS, d/nikola.doc-base, d/pydist-overrides,
    d/README.source: Drop, no longer needed.
  * Update Standards-Version to 4.5.1.

 -- Unit 193 <unit193@debian.org>  Wed, 03 Feb 2021 19:38:15 -0500

nikola (7.6.4-1) unstable; urgency=medium

  * Add Recommends on python-ws4py and python-watchdog for `nikola auto`
    (Closes: #795769)
  * Set version number on (build)?dependencies
  * Imported Upstream version 7.6.4

 -- Agustin Henze <tin@debian.org>  Wed, 26 Aug 2015 07:43:19 -0300

nikola (7.6.0-3) unstable; urgency=medium

  * Added bootstrap(3)-jinja themes missing pieces (Closes: #793953).

 -- Ulises Vitulli <dererk@debian.org>  Wed, 29 Jul 2015 22:38:10 -0300

nikola (7.6.0-2) unstable; urgency=medium

  * Add missing build-dep on python-requests (Closes: #792352).
  * Bind to python-yapsy specific version (required by nikola).

 -- Ulises Vitulli <dererk@debian.org>  Wed, 22 Jul 2015 06:20:14 -0300

nikola (7.6.0-1) unstable; urgency=medium

  * Add missing patch (Closes: #790710)
  * Imported Upstream version 7.6.0 (Closes: #789072, #790906)
  * Replace strict dependency on dateutil for the current one in debian
    archive (Closes:  #767536)
  * Fix manpage path

 -- Agustin Henze <tin@debian.org>  Wed, 08 Jul 2015 09:20:02 -0300

nikola (7.1.0-2) unstable; urgency=medium

  * Add suggestion on python-pygal for easy Charts.

 -- Ulises Vitulli <dererk@debian.org>  Fri, 20 Feb 2015 06:37:25 -0300

nikola (7.1.0-1) unstable; urgency=medium

  * Imported Upstream version 7.1.0
  * Remove patches for bootstrap3
  * Add dependency on libjs-bootstrap
  * Add symlinks to bootstrap3 debian package
  * Fix get-orig-source to import bootstrap3 theme
  * Fix quoting when generate zsh completion (Closes: #762241)
  * Bumped Standard-Version to 3.9.6 (no changes required)

 -- Agustin Henze <tin@debian.org>  Tue, 21 Oct 2014 10:47:58 -0300

nikola (7.0.1-4) unstable; urgency=medium

  * Add missing build dependency on python-dateutil

 -- Agustin Henze <tin@debian.org>  Fri, 08 Aug 2014 08:06:18 -0300

nikola (7.0.1-3) unstable; urgency=medium

  * Remove depends on obsolete python-support
  * Remove echo sentence from nikola.sh (Closes: #757352)

 -- Agustin Henze <tin@debian.org>  Thu, 07 Aug 2014 10:47:12 -0300

nikola (7.0.1-2) unstable; urgency=medium

  * Add nikola.sh (nikola runner to create sh_completion files) (Closes:
    #752954)

 -- Agustin Henze <tin@debian.org>  Wed, 06 Aug 2014 19:10:19 -0300

nikola (7.0.1-1) unstable; urgency=medium

  * Imported Upstream version 7.0.1
  * Update patch to add install_bs3 command
  * Add patch to replace natsort python module by naturalsort
  * Add html5shiv.js to debian/missing-sources
  * Update debian/copyright file
  * Remove depends on python-tz and add depends on python-naturalsort
  * Update debian/rules

 -- Agustin Henze <tin@debian.org>  Mon, 16 Jun 2014 15:28:28 -0300

nikola (6.4.0-1) unstable; urgency=medium

  * New upstream release version 6.4.0
  * Update patch of the custom debian install_bs3 command

 -- Agustin Henze <tin@debian.org>  Sun, 09 Mar 2014 03:22:30 +0100

nikola (6.3.0-1) unstable; urgency=medium

  * New upstream release version 6.3.0
  * Add the awesome patch by Chris "kwpolska" Warrick (add install_bs3
    command) (Closes: #734613)
  * Add bash and zsh completion
  * Use pybuild as buildsystem

 -- Agustin Henze <tin@debian.org>  Fri, 28 Feb 2014 11:33:42 -0300

nikola (6.2.1-1) unstable; urgency=low

  * Imported Upstream version 6.2.1 (Closes: #722177)
  * Updating debian/watch file to the new url of the project
  * Add get-orig-source target
  * Change the default theme bootstrap3 by bootstrap
  * Update dependencies, recommendations and suggestions

 -- Agustin Henze <tin@debian.org>  Mon, 18 Nov 2013 19:26:32 -0300

nikola (5.4.4-1) unstable; urgency=low

  [ Agustin Henze ]
  * Imported Upstream version 5.4.4
  * debian/copyright:
    - Converted to machine readable.
    - Update copyrights.
  * debian/rules:
    - Fix wrong permission files
    - Remove a copy of MIT License
    - Remove the command that deletes a file that is no longer part of the
      project.
  * Update debian/README.source file with the new file differences.

  [ Dererk ]
  * Recommend py-requests for install_theme feature (Closes: #706658).

 -- Agustin Henze <tin@sluc.org.ar>  Fri, 31 May 2013 20:50:03 -0300

nikola (5.4.2-1) unstable; urgency=low

  * Imported Upstream version 5.4.2
  * Removed unused manuals under debian folder
  * Updated README.source file with details about the files deleted for make
    the package dfsg compliant
  * Fixed lintian warning about non-canonical vcs field

 -- Agustin Henze <tin@sluc.org.ar>  Wed, 13 Mar 2013 21:31:44 -0300

nikola (5.3-1) unstable; urgency=low

  * Imported Upstream version 5.3
  * Updated debian/rules:
    - Removed the copy of the custom.css file, fixed in upstream.

 -- Agustin Henze <tin@sluc.org.ar>  Wed, 27 Feb 2013 17:11:04 -0300

nikola (5.2-1) unstable; urgency=low

  * Imported Upstream version 5.2 (Closes: #696358, #699910).
  * Bump Standards-Version to 3.9.4 (no changes required).
  * Added libjs-jquery-slides dependency.
    - Removed slides.jquery.js file of the upstream installation.
  * Updated copyright file:
    - The license of nikola has changed from GPL-3 to MIT.
    - Added license of slides.jquery.js file.
  * Fixed assets symlinks on debian/nikola.links.
  * Updated debian/rules:
    - Deleted the remove sentence of PyRSS2Gen, fixed in upstream.
    - Copy custom.css into the right place, it'll be reported to upstream.
    - Added conversion from rst to html of the docs/creating-a-theme.txt file.
  * Created debian/pydist-overrides: pillow overridden by python-imaging.
  * Added Vcs-git and Vcs-browser to debian/control file.

 -- Agustin Henze <tin@sluc.org.ar>  Wed, 13 Feb 2013 18:42:55 -0300

nikola (5-1) unstable; urgency=low

  * New upstream release, adds new features and fixes several bugs:
    Features:
     - Templates now know their name from template_name in their context.
     - Made most of Nikola into Yapsy plugins.
     - Recurse galleries/ and render each folder as a gallery.
     - Breadcrumbs and folder links in image galleries.
    Bugfixes:
     - Fixed doc installation
     - Put webassets cache in cache/webassets
     - Don't crash on incomplete translations
     - Fixed Issue 160: render_tags didn't call scan_posts()
     - Fixed Issue 161: webassets setting USE_BUNDLES was ignored
     - Fixed Issue 153: index.txt was being ignored in galleries.

 -- Agustin Henze <tin@sluc.org.ar>  Wed, 12 Dec 2012 20:33:41 -0300

nikola (4.0.3-2) unstable; urgency=low

  * Fix broken symlinks not being included on the repkg source.

 -- Agustin Henze <tin@sluc.org.ar>  Mon, 19 Nov 2012 20:34:44 -0300

nikola (4.0.3-1) unstable; urgency=low

  * Initial release (Closes: #690159).

 -- Agustin Henze <tin@sluc.org.ar>  Wed, 09 Oct 2012 18:10:41 -0300
