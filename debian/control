Source: nikola
Section: python
Priority: optional
Maintainer: Unit 193 <unit193@unit193.net>
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 bash-completion,
 python3-all,
 python3-aiohttp <!nocheck>,
 python3-babel <!nocheck>,
 python3-blinker,
 python3-dateutil <!nocheck>,
 python3-doit (>= 0.32.0~),
 python3-docutils,
 python3-feedparser <!nocheck>,
 python3-freezegun <!nocheck>,
 python3-ipykernel <!nocheck>,
 python3-jinja2 <!nocheck>,
 python3-lxml,
 python3-mako,
 python3-markdown <!nocheck>,
 python3-natsort,
 python3-notebook <!nocheck>,
 python3-phpserialize <!nocheck>,
 python3-piexif,
 python3-pil <!nocheck>,
 python3-pygments,
 python3-pyrss2gen,
 python3-pytest <!nocheck>,
 python3-pytest-cov <!nocheck>,
 python3-requests,
 python3-ruamel.yaml <!nocheck>,
 python3-setuptools,
 python3-toml <!nocheck>,
 python3-typogrify <!nocheck>,
 python3-unidecode,
 python3-watchdog <!nocheck>,
 python3-yaml <!nocheck>,
Rules-Requires-Root: no
Standards-Version: 4.7.0
Homepage: https://getnikola.com/
Vcs-Git: https://git.unit193.net/cgit/users/unit193/nikola.git
Vcs-Browser: https://git.unit193.net/cgit/users/unit193/nikola.git

Package: nikola
Architecture: all
Depends:
 ${python3:Depends},
 ${misc:Depends},
 libjs-bootstrap4,
 libjs-jquery,
# libjs-popper.js,
 node-html5shiv,
Recommends:
 python3-typogrify,
 python3-ws4py,
 python3-watchdog,
Suggests:
 python3-jinja2,
 python3-feedparser,
 ipython,
 bpython,
 pandoc,
 python3-textile,
 txt2tags,
 node-less,
 python3-pygal,
Description: simple yet powerful and flexible static website and blog generator
 Nikola is a static site and blog generator. Feed information to it on one
 side and get a ready-to-deploy website on the other.
 .
 Nikola goodies on generating static pages are, between others, that:
  * Static websites are safer, no database nor dynamic code interpreter needed
  * They use fewer resources
  * You avoid vendor and platform lockin.
 .
 Between some of Nikola's features you may find:
    * Blog generator, including tags, feeds, archives, comments, etc.
    * Theme support
    * Fast building process, thanks to doit
    * Easy Image Gallery, just drop files in a folder!
    * reStructuredText or Markdown as Input languages
    * Flexible design
    * Small codebase.
    * Syntax highlighting for almost any programming language or markup
    * Multi-language sites
    * Small, since based on leverages existing tools
    * Between others
